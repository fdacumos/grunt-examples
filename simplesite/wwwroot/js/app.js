Hello = function(){

  var helloWorld = function() {
    return "Hello World!";
  }

  return {
    /**
     * Gets a Hello World message
     */
    getHelloWorld: function() {
      return helloWorld();
    }
  }
}
