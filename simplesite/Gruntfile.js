module.exports = function(grunt) {
  // 1. Configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    concat: {
      // 2. Configuration for concatinating files goes here
      dist: {
        src: [
          'node_modules/jquery/dist/jquery.js',
          'wwwroot/js/app.js'
        ],
        dest: 'wwwroot/js/production.js'
      }
    },

    uglify: {
      build: {
        src: 'wwwroot/js/production.js',
        dest: 'wwwroot/js/production.min.js'
      }
    },

    processhtml: {
      dist: {
        files: {
          'wwwroot/index.html':['wwwroot/index.html']
        }
      }
    }
  });

  // 3. Where grunt loads the plug-ins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-processhtml');

  // 4. Tasks run when "grunt" is typed at the command line
  grunt.registerTask('default', ['concat','uglify','processhtml']);

};
